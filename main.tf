# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

resource "kubernetes_namespace" "workflows_informer" {
  metadata {
    name = "workflows-informer"
    labels = {
      # https://kubernetes.io/docs/concepts/security/pod-security-standards/
      "pod-security.kubernetes.io/enforce" = "restricted"
    }
  }
}

# https://artifacthub.io/packages/helm/workflows-informer/workflows-informer
resource "helm_release" "workflows_informer" {
  name       = "workflows-informer"
  namespace  = kubernetes_namespace.workflows_informer.metadata[0].name
  repository = "oci://registry.gitlab.com/dyff/charts"
  chart      = "workflows-informer"
  version    = "0.4.2"

  wait = false

  values = [yamlencode({
    extraEnvVarsConfigMap = {
      DYFF_KAFKA__CONFIG__BOOTSTRAP_SERVERS = local.kafka.bootstrap_servers
      DYFF_KAFKA__TOPICS__WORKFLOWS_EVENTS  = local.kafka.topics_map["dyff.workflows.events"].name
    }

    replicaCount = 1

    resources = {
      limits = {
        cpu                 = "750m"
        memory              = "768Mi"
        "ephemeral-storage" = "1024Mi"
      }
      requests = {
        cpu                 = "500m"
        memory              = "512Mi"
        "ephemeral-storage" = "50Mi"
      }
    }
  })]
}
